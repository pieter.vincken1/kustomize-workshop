# kustomize-workshop

Ordina BE workshop for kustomize.

## Overview

In this workshop the following topics will be discussed:

1. What is Kustomize?
1. When to use Kustomize?
1. Simple example
1. Exercise 1: single base with environment specific overlays
1. Exercise 2: multi-base overlay with generic overrides (regex patch)
1. Bonus: Plugins

https://slides.com/pietervincken